package org.zeith.cableflux.client;

import com.zeitheron.hammercore.client.render.item.IItemRender;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.TextureAtlasSpriteFull;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.zeith.cableflux.pipes.IPipeImage;
import org.zeith.cableflux.pipes.IPipeRenderer;

public class TESRPipe
		implements IItemRender
{
	/*
	@Override
	public void renderTileEntityAt(TilePipe te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		RenderBlocks rb = RenderBlocks.forMod("cableflux");
		Tessellator tess = Tessellator.getInstance();

		GL11.glPushMatrix();
		GlStateManager.disableLighting();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.enableBlend();
		GlStateManager.enableAlpha();

		GlStateManager.shadeModel(Minecraft.isAmbientOcclusionEnabled() ? 7425 : 7424);

		GL11.glTranslated(x, y, z);
		int bri = getBrightnessForRB(te, rb);
		tess.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
		renderPipe(te.getPipe(), rb, bri, 1, 1, 1, alpha, partialTicks);
		tess.draw();
		RenderHelper.enableStandardItemLighting();
		GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityFast(TilePipe te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, BufferBuilder buffer)
	{
		RenderBlocks rb = RenderBlocks.forMod("cableflux");
		Tessellator tess = Tessellator.getInstance();
		int bri = getBrightnessForRB(te, rb);
		renderPipe(te.getPipe(), rb, bri, 1, 1, 1, 1F, partialTicks);
	}
	 */

	@Override
	public void renderItem(ItemStack item)
	{
		IPipeImage pipe = Cast.cast(Block.getBlockFromItem(item.getItem()), IPipeImage.class);
		if(pipe != null)
		{
			RenderBlocks rb = RenderBlocks.forMod("cableflux");
			Tessellator tess = Tessellator.getInstance();

			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			int bri = 250;
			if(Minecraft.getMinecraft().world != null && Minecraft.getMinecraft().player != null)
				bri = rb.setLighting(Minecraft.getMinecraft().world, Minecraft.getMinecraft().player.getPosition());
			tess.getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
			renderPipe(pipe, rb, bri, 1, 1, 1, 1, Minecraft.getMinecraft().getRenderPartialTicks());
			tess.draw();
			GL11.glPopMatrix();
			GlStateManager.enableLighting();
		}
	}

	private void renderPipe(IPipeImage pipe, RenderBlocks rb, int bright, float r, float g, float b, float a, float partialTicks)
	{
		if(pipe == null)
			return;

		if(pipe instanceof IPipeRenderer)
		{
			((IPipeRenderer) pipe).doRender(pipe, rb, bright, a, partialTicks);
			return;
		}

		try
		{
			ResourceLocation tex = pipe.getTexture();
			if(tex != null)
				UtilsFX.bindTexture(new ResourceLocation(tex.getNamespace(), "textures/" + tex.getPath() + "_isr.png"));
			else
				return;
		} catch(Throwable err)
		{
		}

		double x = -1 / 16. + 5 / 16D;
		double y = -1 / 16. + 5 / 16D;
		double z = -1 / 16. + 5 / 16D;

		// Core

		rb.setRenderBounds(1 / 16D, 1 / 16D, 1 / 16D, 7 / 16D, 7 / 16D, 7 / 16D);
		rb.renderFaceYNeg(x, y, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		rb.renderFaceYPos(x, y, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);

		rb.setRenderBounds(1 / 16D, 9 / 16D, 1 / 16D, 7 / 16D, 15 / 16D, 7 / 16D);
		rb.renderFaceXNeg(x, y - 8 / 16D, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		rb.renderFaceXPos(x, y - 8 / 16D, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		rb.renderFaceZNeg(x, y - 8 / 16D, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		rb.renderFaceZPos(x, y - 8 / 16D, z, TextureAtlasSpriteFull.sprite, r, g, b, bright);

		// Down
		if(pipe.isConnectedTo(EnumFacing.DOWN))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 1 / 16D, 6 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 6 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 6 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZNeg(x + .5 / 16, y - 6 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZPos(x + .5 / 16, y - 6 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x - 7.5 / 16, y - 6 / 16D, z - 7.5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}

		// Up
		if(pipe.isConnectedTo(EnumFacing.UP))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 1 / 16D, 6 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y + 5 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y + 5 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZNeg(x + .5 / 16, y + 5 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZPos(x + .5 / 16, y + 5 / 16D, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYPos(x - 7.5 / 16, y + 5 / 16D, z - 7.5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}

		// North
		if(pipe.isConnectedTo(EnumFacing.NORTH))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 9 / 16D, 6 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x + .5 / 16, y - .5 / 16, z - 13. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceYPos(x + .5 / 16, y - .5 / 16, z - 13. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(1 / 16D, 10 / 16D, 9 / 16D, 6 / 16D, 15 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 8.5 / 16, z - 13. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 8.5 / 16, z - 13. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceZNeg(x - 7.5 / 16, y - .5 / 16, z - 13. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}

		// South
		if(pipe.isConnectedTo(EnumFacing.SOUTH))
		{
			rb.setRenderBounds(1 / 16D, 2 / 16D, 9 / 16D, 6 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceYNeg(x + .5 / 16, y - .5 / 16, z - 2. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceYPos(x + .5 / 16, y - .5 / 16, z - 2. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(1 / 16D, 10 / 16D, 9 / 16D, 6 / 16D, 15 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x + .5 / 16, y - 8.5 / 16, z - 2. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceXPos(x + .5 / 16, y - 8.5 / 16, z - 2. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceZPos(x - 7.5 / 16, y - .5 / 16, z - 2. / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}

		// East
		if(pipe.isConnectedTo(EnumFacing.EAST))
		{
			rb.setRenderBounds(9 / 16D, 2 / 16D, 1 / 16D, 14 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceYNeg(x - 2. / 16, y - .5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceYPos(x - 2. / 16, y - .5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 10 / 16D, 1 / 16D, 14 / 16D, 15 / 16D, 6 / 16D);
			rb.renderFaceZNeg(x - 2. / 16, y - 8.5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZPos(x - 2. / 16, y - 8.5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceXPos(x - 2. / 16, y - .5 / 16, z - 7.5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}

		// West
		if(pipe.isConnectedTo(EnumFacing.WEST))
		{
			rb.setRenderBounds(9 / 16D, 2 / 16D, 1 / 16D, 14 / 16D, 7 / 16D, 6 / 16D);
			rb.renderFaceYNeg(x - 13. / 16, y - .5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceYPos(x - 13. / 16, y - .5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 10 / 16D, 1 / 16D, 14 / 16D, 15 / 16D, 6 / 16D);
			rb.renderFaceZNeg(x - 13. / 16, y - 8.5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
			rb.renderFaceZPos(x - 13. / 16, y - 8.5 / 16, z + .5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);

			rb.setRenderBounds(9 / 16D, 2 / 16D, 9 / 16D, 14 / 16D, 7 / 16D, 14 / 16D);
			rb.renderFaceXNeg(x - 13. / 16, y - .5 / 16, z - 7.5 / 16, TextureAtlasSpriteFull.sprite, r, g, b, bright);
		}
	}
}