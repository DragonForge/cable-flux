package org.zeith.cableflux.init;

import net.minecraft.util.ResourceLocation;

public class TexturesCF
{
	public static final ResourceLocation FE_1 = new ResourceLocation("cableflux:pipes/fe.1");
	public static final ResourceLocation FE_2 = new ResourceLocation("cableflux:pipes/fe.2");
	public static final ResourceLocation FE_3 = new ResourceLocation("cableflux:pipes/fe.3");

	public static final ResourceLocation FL_1 = new ResourceLocation("cableflux:pipes/fl.1");
}