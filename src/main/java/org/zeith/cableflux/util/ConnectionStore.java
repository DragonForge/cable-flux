package org.zeith.cableflux.util;

public class ConnectionStore
{
	public static byte wrap(boolean[] bs)
	{
		return (byte) (
				to(bs[0], 0x01)
						| to(bs[1], 0x02)
						| to(bs[2], 0x03)
						| to(bs[3], 0x04)
						| to(bs[4], 0x05)
						| to(bs[5], 0x06)
		);
	}
	
	private static int to(boolean bit, int shift)
	{
		return bit ? 1 << shift : 0;
	}
	
	public static void unwrap(byte x, boolean[] bs)
	{
		bs[0] = ((x >> 0x01) & 0x1) != 0;
		bs[1] = ((x >> 0x02) & 0x1) != 0;
		bs[2] = ((x >> 0x03) & 0x1) != 0;
		bs[3] = ((x >> 0x04) & 0x1) != 0;
		bs[4] = ((x >> 0x05) & 0x1) != 0;
		bs[5] = ((x >> 0x06) & 0x1) != 0;
	}
}