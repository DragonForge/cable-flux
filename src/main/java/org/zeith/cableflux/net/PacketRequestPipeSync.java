package org.zeith.cableflux.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import org.zeith.cableflux.pipes.IPipe;
import org.zeith.cableflux.tiles.TilePipe;

public class PacketRequestPipeSync
		implements IPacket
{
	long pos;
	
	public static PacketRequestPipeSync create(IPipe pipe)
	{
		PacketRequestPipeSync req = new PacketRequestPipeSync();
		req.pos = pipe.getCoords().toLong();
		return req;
	}
	
	public PacketRequestPipeSync()
	{
	}
	
	public PacketRequestPipeSync(BlockPos pos)
	{
		this.pos = pos.toLong();
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("Pos", pos);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		pos = nbt.getLong("Pos");
	}
	
	@Override
	public void executeOnServer2(PacketContext net)
	{
		EntityPlayerMP mp = net.getSender();
		if(mp != null)
		{
			WorldServer world = mp.getServerWorld();
			BlockPos pos = BlockPos.fromLong(this.pos);
			
			TilePipe holder = Cast.cast(world.getTileEntity(pos), TilePipe.class);
			if(holder != null)
			{
				PacketPerformPipeSync pkt = new PacketPerformPipeSync();
				pkt.nbt = holder.getUpdateTag();
				pkt.pos = this.pos;
				net.withReply(pkt);
			}
		}
	}
}