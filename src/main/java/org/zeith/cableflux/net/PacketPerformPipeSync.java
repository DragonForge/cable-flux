package org.zeith.cableflux.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.tiles.TilePipe;

public class PacketPerformPipeSync
		implements IPacket
{
	public NBTTagCompound nbt;
	public long pos;
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setTag("n", this.nbt);
		nbt.setLong("p", pos);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.nbt = nbt.getCompoundTag("n");
		pos = nbt.getLong("p");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void executeOnClient2(PacketContext net)
	{
		World world = Minecraft.getMinecraft().world;
		if(world != null)
		{
			TilePipe holder = Cast.cast(world.getTileEntity(BlockPos.fromLong(pos)), TilePipe.class);
			if(holder != null)
			{
				holder.handleUpdateTag(nbt);
			}
		}
	}
}