package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.cfg.ConfigCF;
import org.zeith.cableflux.tiles.TilePipe;

public abstract class BasePipe
		implements IPipe
{
	public boolean isDead;
	public PipeGrid grid;
	public BlockPos pos;
	public World world;
	public TilePipe tile;
	protected boolean scheduledReRender;
	
	public abstract boolean testConnectionTo(EnumFacing facing, IPipe other);
	
	@Override
	public void initTile(TilePipe pipe)
	{
		this.tile = pipe;
		IPipe.super.initTile(pipe);
	}
	
	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return tile.connections[face.ordinal()];
	}
	
	@Override
	public void neighborChanged(TilePipe tile)
	{
		World world;
		if(tile.hasWorld() && !(world = tile.getWorld()).isRemote)
		{
			for(EnumFacing facing : EnumFacing.VALUES) // Updates connections server-side;
			{
				boolean prev = tile.connections[facing.ordinal()];
				
				IPipe rem = PipeManager.getPipe(world, pos.offset(facing));
				
				tile.connections[facing.ordinal()] = testConnectionTo(facing, rem);
				if(tile.connections[facing.ordinal()] != prev)
				{
					scheduledReRender = true;
					if(rem != null)
					{
						BlockPos pos = rem.getCoords();
						TilePipe tp = Cast.cast(world.getTileEntity(pos), TilePipe.class);
						if(tp != null)
							tp.scheduleConnectionRefresh(2);
					}
				}
			}
			
			if(scheduledReRender)
			{
//				tile.scheduleConnectionRefresh(2);
			}
		}
	}
	
	@Override
	public void update(TilePipe tile)
	{
		if(tile.atGlobalTickRate(30))
			neighborChanged(tile);
		
		if(scheduledReRender && world != null)
		{
			IBlockState state;
			world.notifyBlockUpdate(pos, state = getWorld().getBlockState(pos), state, 3);
			scheduledReRender = false;
		}
		
		IPipe.super.update(tile);
	}
	
	@Override
	public void write(NBTTagCompound nbt)
	{
		IPipe.super.write(nbt);
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		scheduledReRender = true;
		IPipe.super.read(nbt);
	}
	
	@Override
	public BlockPos getCoords()
	{
		return pos;
	}
	
	@Override
	public void setCoords(BlockPos pos)
	{
		this.pos = pos;
	}
	
	@Override
	public World getWorld()
	{
		return world;
	}
	
	@Override
	public void setWorld(World world)
	{
		this.world = world;
	}
	
	@Override
	public void kill()
	{
		isDead = true;
	}
	
	@Override
	public boolean isDead()
	{
		return isDead;
	}
	
	@Override
	public PipeGrid getGrid()
	{
		return grid;
	}
	
	@Override
	public void setGrid(PipeGrid grid)
	{
		this.grid = grid;
	}
	
	long lastSyncOnLook;
	
	@SideOnly(Side.CLIENT)
	public void hookTooltip(ITooltip tooltip)
	{
		if(ConfigCF.showHUD)
		{
			if(System.currentTimeMillis() - lastSyncOnLook > 100L)
			{
				lastSyncOnLook = System.currentTimeMillis();
				PipeSync.requestSyncFromClient(this);
			}
			addTooltip(tooltip);
		}
	}
}