package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class PipeGrid
{
	public IPipe masterPipe;
	public World world;
	public List<BlockPos> pipes = new ArrayList<>();
	
	public static boolean isPipeAt(World world, BlockPos pos, Class<? extends IPipe> type)
	{
		IPipe pipe = PipeManager.getPipe(world, pos);
		return pipe != null && type.isAssignableFrom(pipe.getClass());
	}
	
	public static PipeGrid defineGrid(World world, BlockPos pos)
	{
		PipeGrid grid = new PipeGrid();
		grid.world = world;
		
		if(isPipeAt(world, pos, IPipe.class))
		{
			IPipe pipe = PipeManager.getPipe(world, pos);
			
			PipeGrid cgrid = pipe.createCustomGrid();
			if(cgrid != null)
			{
				cgrid.world = world;
				grid = cgrid;
			}
			
			grid.pipes.add(pos);
			grid.masterPipe = pipe;
			grid.masterPipe.setGrid(grid);
			if(grid.masterPipe == null)
				return null;
		} else
			return null;
		
		for(int i = 0; i < grid.pipes.size(); ++i)
			grid.expandGrid(grid.pipes.get(i));
		
		grid.orderGrid();
		
		return grid;
	}
	
	public void expandGrid(BlockPos from)
	{
		if(pipes.contains(from))
		{
			IPipe cur = PipeManager.getPipe(world, from);
			
			for(EnumFacing face : EnumFacing.VALUES)
			{
				BlockPos op = from.offset(face);
				IPipe p = PipeManager.getPipe(world, op);
				if(p != null && cur.canAddToSameGrid(p) && !pipes.contains(op))
				{
					pipes.add(op);
					p.setGrid(this);
				}
			}
		}
	}
	
	public <T> Stream<? extends T> streamOfPipes(Class<T> type)
	{
		return pipes
				.stream()
				.map(pos -> Cast.cast(PipeManager.getPipe(world, pos), type))
				.filter(Objects::nonNull);
	}
	
	/**
	 * Removes invalid pipes from the list and sorts it.
	 */
	public void orderGrid()
	{
		pipes.removeIf(pos -> PipeManager.getPipe(world, pos) == null);
		pipes.sort((a, b) -> (int) a.distanceSq(b));
	}
	
	public IPipe getMasterPipe()
	{
		return masterPipe;
	}
	
	/**
	 * Marks that this grid was physically reshaped. Re-discovers all of it's
	 * components
	 */
	public void majorGridChange()
	{
		orderGrid();
		if(pipes.isEmpty())
			return;
		masterPipe = PipeManager.getPipe(world, pipes.get(0));
		if(masterPipe == null)
			return;
		pipes.clear();
		pipes.add(masterPipe.getCoords());
		for(int i = 0; i < pipes.size(); ++i)
			expandGrid(pipes.get(i));
		orderGrid();
		getMasterPipe().balanceGrid();
	}
	
	/**
	 * Adds a pipe to this grid.
	 */
	public void addPipe(IPipe pipe)
	{
		if(pipe != null)
		{
			PipeGrid og = pipe.getGrid();
			
			if(og != null)
			{
				og.pipes.forEach(pos ->
				{
					IPipe ip = PipeManager.getPipe(world, pos);
					if(ip != null)
						ip.setGrid(null);
				});
				og.pipes.clear();
			}
			
			if(!pipes.contains(pipe.getCoords()))
			{
				pipes.add(pipe.getCoords());
				pipe.setGrid(this);
				majorGridChange();
			}
		}
	}
}