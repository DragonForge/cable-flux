package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.utils.PositionedStateImplementation;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.cfg.ConfigCF;
import org.zeith.cableflux.client.BakedPipeModel;
import org.zeith.cableflux.tiles.TilePipe;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface IPipe
		extends IPipeImage
{
	/**
	 * All cuboids for hitboxes that any pipe has. These are CONSTANTS.
	 */
	Cuboid6 CENTER = new Cuboid6(5 / 16D, 5 / 16D, 5 / 16D, 11 / 16D, 11 / 16D, 11 / 16D),
			DOWN_CENTER = new Cuboid6(5.5 / 16D, 0, 5.5 / 16D, 10.5 / 16D, 5 / 16D, 10.5 / 16D),
			CENTER_UP = new Cuboid6(5.5 / 16D, 10.5 / 16D, 5.5 / 16D, 10.5 / 16D, 1, 10.5 / 16D),
			DOWN_UP = new Cuboid6(5.5 / 16D, 0, 5.5 / 16D, 10.5 / 16D, 1, 10.5 / 16D),
			NORH_CENTER = new Cuboid6(5.5 / 16D, 5.5 / 16D, 0, 10.5 / 16D, 10.5 / 16D, 5 / 16D),
			CENTER_SOUTH = new Cuboid6(5.5 / 16D, 5.5 / 16D, 10.5 / 16D, 10.5 / 16D, 10.5 / 16D, 1),
			NORTH_SOUTH = new Cuboid6(5.5 / 16D, 5.5 / 16D, 0, 10.5 / 16D, 10.5 / 16D, 1),
			WEST_CENTER = new Cuboid6(0, 5.5 / 16D, 5.5 / 16D, 5 / 16D, 10.5 / 16D, 10.5 / 16D),
			CENTER_EAST = new Cuboid6(10.5 / 16D, 5.5 / 16D, 5.5 / 16D, 1, 10.5 / 16D, 10.5 / 16D),
			WEST_EAST = new Cuboid6(0, 5.5 / 16D, 5.5 / 16D, 1, 10.5 / 16D, 10.5 / 16D);
	
	/**
	 * Used when joining pipes into same grid through neighbors.
	 */
	default boolean canAddToSameGrid(IPipe other)
	{
		if(other != null && getClass().isAssignableFrom(other.getClass()))
			return true;
		return false;
	}
	
	default void initTile(TilePipe pipe)
	{
	}
	
	/**
	 * Gets the coordinates of this pipe in the world.
	 */
	BlockPos getCoords();
	
	/**
	 * Set the coordinates of this pipe in the world.
	 */
	void setCoords(BlockPos pos);
	
	/**
	 * Gets the world that this pipe is in.
	 */
	World getWorld();
	
	/**
	 * Sets the world that this pipe is in.
	 */
	void setWorld(World world);
	
	/**
	 * Gets the grid that this pipe belongs to.
	 */
	PipeGrid getGrid();
	
	/**
	 * Sets the grid that this pipe belongs to.
	 */
	void setGrid(PipeGrid grid);
	
	/**
	 * Marks this pipe as 'dead'.
	 */
	void kill();
	
	/**
	 * Gets if this pipe is marked as 'dead'.
	 */
	boolean isDead();
	
	/**
	 * Called when this pipe should probably recalculate its connections.
	 */
	default void neighborChanged(TilePipe tile)
	{
	}
	
	/**
	 * Called each tick to update this pipe.
	 */
	default void update(TilePipe tile)
	{
		setCoords(tile.getPos());
		setWorld(tile.getWorld());
		
		PipeGrid grid = getGrid();
		if(grid != null && grid.getMasterPipe() == this && tile.atTickRate(2))
			balanceGrid();
	}
	
	/**
	 * Called only from master pipe to balance the entire grid's energy (or
	 * anything else).
	 */
	default void balanceGrid()
	{
	}
	
	/**
	 * Performs read operation to restore all previously stored data from NBT.
	 */
	default void read(NBTTagCompound nbt)
	{
	}
	
	/**
	 * Performs write operation to store all pipe data to NBT.
	 */
	default void write(NBTTagCompound nbt)
	{
	}
	
	/**
	 * Creates optional drop. Can be anything, really. This heavily depends on
	 * pipe type. Typically called when this pipe is destroyed.
	 */
	default void createDrops(WorldLocation at)
	{
		PipeGrid grid = getGrid();
		if(grid != null)
			grid.majorGridChange();
	}
	
	/**
	 * Internal only. Adds cuboids to hitboxes.
	 */
	default void addCuboids(List<Cuboid6> cubes)
	{
		List<EnumFacing> connected = Arrays.stream(EnumFacing.VALUES).filter(this::isConnectedTo).collect(Collectors.toList());
		
		if(connected.size() == 2)
		{
			EnumFacing.Axis axis = connected.get(0).getAxis();
			if(connected.get(1).getAxis() == axis)
			{
				switch(axis)
				{
					case X:
						cubes.add(WEST_EAST);
						return;
					case Y:
						cubes.add(DOWN_UP);
						return;
					case Z:
						cubes.add(NORTH_SOUTH);
						return;
				}
			}
		}
		
		cubes.add(CENTER);
		
		if(connected.contains(EnumFacing.DOWN)) cubes.add(DOWN_CENTER);
		if(connected.contains(EnumFacing.UP)) cubes.add(CENTER_UP);
		if(connected.contains(EnumFacing.NORTH)) cubes.add(NORH_CENTER);
		if(connected.contains(EnumFacing.SOUTH)) cubes.add(CENTER_SOUTH);
		if(connected.contains(EnumFacing.WEST)) cubes.add(WEST_CENTER);
		if(connected.contains(EnumFacing.EAST)) cubes.add(CENTER_EAST);
	}
	
	@SideOnly(Side.CLIENT)
	default void hookTooltip(ITooltip tooltip)
	{
		if(ConfigCF.showHUD)
			addTooltip(tooltip);
	}
	
	/**
	 * Use to add tooltip to HUD.
	 */
	@SideOnly(Side.CLIENT)
	default void addTooltip(ITooltip tooltip)
	{
	}
	
	/**
	 * A mirror of TE's capability API.
	 */
	default <T> boolean hasCapability(Capability<T> capability, EnumFacing facing)
	{
		return false;
	}
	
	/**
	 * A mirror of TE's capability API.
	 */
	default <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return null;
	}
	
	/**
	 * Client-side method to generate a visible pipe model.
	 */
	@SideOnly(Side.CLIENT)
	default void generateBakedQuads(List<BakedQuad> quads, EnumFacing side, long rand, IBlockAccess world, BlockPos pos, PositionedStateImplementation state, Predicate<EnumFacing> connetions, TextureAtlasSprite sprite)
	{
		BakedPipeModel.generateQuads(quads, side, rand, world, pos, state, connetions, sprite);
	}
	
	/**
	 * Creates an overriden instance of pipe grid. May be useful to store extra bit of info about this grid.
	 *
	 * @return A custom grid, or null, to allow vanilla behavior.
	 */
	default PipeGrid createCustomGrid()
	{
		return null;
	}
}