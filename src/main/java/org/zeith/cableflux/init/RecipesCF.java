package org.zeith.cableflux.init;

import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

@RegisterRecipes(modid = "cableflux")
public class RecipesCF
		extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		shaped(new ItemStack(BlocksCF.FE_1, 6), "ggg", "rrr", "ggg", 'r', "dustRedstone", 'g', "blockGlass");
		shaped(new ItemStack(BlocksCF.FE_2, 6), "ggg", "rrr", "ggg", 'r', "dustRedstone", 'g', "ingotIron");
		shaped(new ItemStack(BlocksCF.FE_3, 2), "ddd", "geg", "ddd", 'd', "blockDiamond", 'g', "dustGlowstone", 'e', Items.ENDER_EYE);
	}
}