package org.zeith.cableflux.proxy;

import com.google.common.base.Predicates;
import com.zeitheron.hammercore.proxy.RenderProxy_Client;
import org.zeith.cableflux.blocks.BlockPipe;
import org.zeith.cableflux.client.BakedPipeModel;
import org.zeith.cableflux.client.TESRPipe;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ClientProxy
		extends CommonProxy
{
	@Override
	public void preInit()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public void init()
	{
		TESRPipe tesr = new TESRPipe();
		ForgeRegistries.BLOCKS.getValuesCollection()
				.stream()
				.filter(Predicates.instanceOf(BlockPipe.class))
				.forEach(pipe ->
				{
					ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(pipe), tesr);
					RenderProxy_Client.bakedModelStore.putConstant(pipe.getDefaultState(), new BakedPipeModel(Cast.cast(pipe)));
				});
	}

	@SubscribeEvent
	public void reloadTextureMap(TextureStitchEvent.Pre e)
	{
		TextureMap map = e.getMap();

		ForgeRegistries.BLOCKS.getValuesCollection()
				.stream()
				.filter(Predicates.instanceOf(BlockPipe.class))
				.map(b -> Cast.cast(b, BlockPipe.class))
				.forEach(pipe -> map.registerSprite(pipe.getTexture()));
	}
}