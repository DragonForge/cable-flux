package org.zeith.cableflux.intr.hammercore;

import org.zeith.cableflux.blocks.BlockPipe;
import com.zeitheron.hammercore.api.mhb.IRayCubeRegistry;
import com.zeitheron.hammercore.api.mhb.IRayRegistry;
import com.zeitheron.hammercore.api.mhb.RaytracePlugin;
import net.minecraft.block.Block;
import net.minecraftforge.fml.common.registry.GameRegistry;

@RaytracePlugin
public class IntegrHammerCore
		implements IRayRegistry
{
	IRayCubeRegistry cubeRegistry;

	@Override
	public void registerCubes(IRayCubeRegistry cube)
	{
		cubeRegistry = cube;
		GameRegistry.findRegistry(Block.class).getValuesCollection().forEach(this::register);
		cubeRegistry = null;
	}

	private void register(Block bl)
	{
		if(bl instanceof BlockPipe)
			cubeRegistry.bindBlockCubeManager((BlockPipe) bl, (BlockPipe) bl);
	}
}