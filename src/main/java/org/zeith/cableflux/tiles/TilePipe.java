package org.zeith.cableflux.tiles;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.blocks.BlockPipe;
import org.zeith.cableflux.pipes.*;
import org.zeith.cableflux.util.ConnectionStore;
import org.zeith.cableflux.util.Scheduler;

import java.util.Map;

public class TilePipe
		extends TileSyncableTickable
		implements ITooltipProviderHC
{
	public final boolean[] connections = new boolean[6];
	public BlockPipe block;
	public IPipe pipe;
	public boolean decodingNetPacket;
	public boolean encodingNetPacket;
	protected boolean initialTick = true;
	public final Scheduler<TilePipe> scheduler = new Scheduler<>();
	
	public void neighborChanged()
	{
		IPipe pipe = getPipe();
		if(pipe != null)
			pipe.neighborChanged(this);
	}
	
	public void scheduleConnectionRefresh(int after)
	{
		scheduler.schedule(after, tile ->
		{
			tile.neighborChanged();
			IBlockState state;
			world.notifyBlockUpdate(pos, state = getWorld().getBlockState(pos), state, 3);
			if(!world.isRemote)
				tile.sync();
		});
	}
	
	@Override
	public void tick()
	{
		scheduler.update(this);
		
		if(block == null)
		{
			BlockPipe bp = Cast.cast(world.getBlockState(pos).getBlock(), BlockPipe.class);
			if(bp != null)
				block = bp;
			else if(!world.isRemote)
			{
				world.removeTileEntity(pos);
				return;
			}
		}
		
		if(!createPipe() && !world.isRemote)
		{
			world.removeTileEntity(pos);
			return;
		}
		
		if(initialTick)
		{
			initialTick = false;
			
			if(world.isRemote)
			{
				// Request more details about the pipe so that connections hopefully get sent.
				scheduler.schedule(10, PipeSync::requestSyncFromClient);
			}
		}
		
		if(pipe != null)
			pipe.update(this);
		
		if(!world.isRemote && (pipe == null || pipe.isDead()))
		{
			BlockPipe bp = Cast.cast(world.getBlockState(pos).getBlock(), BlockPipe.class);
			if(bp != null)
				world.destroyBlock(pos, true);
			else
			{
				if(pipe != null)
					pipe.createDrops(getLocation());
				world.removeTileEntity(pos);
			}
		}
		
		if(atTickRate(2))
			setTooltipDirty(true);
	}
	
	public boolean atGlobalTickRate(int rate)
	{
		return world.getTotalWorldTime() % rate == 0;
	}
	
	@Override
	public NBTTagCompound getUpdateTag()
	{
		encodingNetPacket = true;
		NBTTagCompound tag = new NBTTagCompound();
		writeNBT(tag);
		encodingNetPacket = false;
		return tag;
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		handleUpdateTag(pkt.getNbtCompound());
	}
	
	@Override
	public void handleUpdateTag(NBTTagCompound tag)
	{
		decodingNetPacket = true;
		readNBT(tag);
		decodingNetPacket = false;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setByte("ConnectionMask", ConnectionStore.wrap(connections));
		if(createPipe())
			pipe.write(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		ConnectionStore.unwrap(nbt.getByte("ConnectionMask"), connections);
		if(createPipe())
			pipe.read(nbt);
	}
	
	@Override
	public void addProperties(Map<String, Object> properties, RayTraceResult trace)
	{
		for(EnumFacing f : EnumFacing.VALUES)
		{
			properties.put(f.getName(), pipe != null && pipe.isConnectedTo(f));
		}
	}
	
	private boolean createPipe()
	{
		if(block != null && pipe == null)
		{
			pipe = block.createPipe(this);
			
			if(pipe == null)
				return false;
			
			if(pipe.getWorld() == null)
				pipe.setWorld(readNBT_world != null ? readNBT_world : world);
			if(pipe.getCoords() == null)
				pipe.setCoords(pos);
			
			for(EnumFacing face : EnumFacing.VALUES)
			{
				IPipe p = PipeManager.getPipe(world, pos.offset(face));
				if(p != null && p.canAddToSameGrid(p) && p.getGrid() != null)
				{
					p.getGrid().addPipe(pipe);
					p.neighborChanged(this);
				}
			}
			
			// Creates a new grid if unable to join current.
			if(pipe.getGrid() == null)
				PipeGrid.defineGrid(world, pos);
			
			neighborChanged();
		}
		
		if(pipe != null && pipe.getWorld() == null)
			pipe.setWorld(readNBT_world != null ? readNBT_world : world);
		
		return pipe != null;
	}
	
	public IPipe getPipe()
	{
		createPipe();
		return pipe;
	}
	
	boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		if(pipe != null)
			pipe.hookTooltip(tip);
	}
}