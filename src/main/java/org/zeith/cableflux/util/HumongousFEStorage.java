package org.zeith.cableflux.util;

import com.zeitheron.hammercore.utils.energy.BigEnergyStorage;
import com.zeitheron.hammercore.utils.math.BigMath;
import net.minecraft.nbt.NBTTagCompound;

import java.math.BigInteger;

public class HumongousFEStorage extends BigEnergyStorage
{
	public HumongousFEStorage(BigInteger capacity)
	{
		super(capacity);
	}
	
	public HumongousFEStorage(BigInteger capacity, BigInteger maxTransfer)
	{
		super(capacity, maxTransfer);
	}
	
	public HumongousFEStorage(BigInteger capacity, BigInteger maxReceive, BigInteger maxExtract)
	{
		super(capacity, maxReceive, maxExtract);
	}
	
	public HumongousFEStorage(BigInteger capacity, BigInteger maxReceive, BigInteger maxExtract, BigInteger energy)
	{
		super(capacity, maxReceive, maxExtract, energy);
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Energy", BigMath.max(energy, BigInteger.ZERO).toString(16));
		nbt.setString("Capacity", BigMath.max(capacity, BigInteger.ZERO).toString(16));
		return nbt;
	}
	
	@Override
	public HumongousFEStorage readFromNBT(NBTTagCompound nbt)
	{
		try
		{
			energy = new BigInteger(nbt.getString("Energy"), 16);
		} catch(NumberFormatException ignored)
		{
			energy = BigInteger.ZERO;
		}
		
		try
		{
			capacity = new BigInteger(nbt.getString("Capacity"), 16);
		} catch(NumberFormatException ignored)
		{
		}
		
		return this;
	}
}