package org.zeith.cableflux.pipes.impl;

import com.zeitheron.hammercore.tile.tooltip.SimpleProgressBar;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ProgressBarTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.pipes.BasePipe;
import org.zeith.cableflux.pipes.IPipe;
import org.zeith.cableflux.pipes.PipeGrid;
import org.zeith.cableflux.tiles.TilePipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class FluidPipe
		extends BasePipe
{
	public FluidTank tank;
	public int capacity;
	public ResourceLocation texture;

	public FluidPipe(ResourceLocation texture, int capacity)
	{
		this.tank = new FluidTank(capacity);
		this.texture = texture;
		this.capacity = capacity;
	}

	public FluidPipe setTexture(ResourceLocation texture)
	{
		this.texture = texture;
		return this;
	}

	@Override
	public void update(TilePipe tile)
	{
		if(tank.getFluidAmount() > 0)
			for(EnumFacing face : EnumFacing.VALUES)
			{
				TileEntity te = tile.getWorld().getTileEntity(tile.getPos().offset(face));
				if(te != null && !(te instanceof TilePipe) && te.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face.getOpposite()))
				{
					IFluidHandler es = te.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face.getOpposite());
					for(IFluidTankProperties prop : es.getTankProperties())
					{
						if(prop.canFillFluidType(tank.getFluid()))
						{
							FluidStack stack = gatherGrid();
							if(stack != null)
							{
								stack = stack.copy();
								stack.amount = es.fill(stack.copy(), true);
								drainGrid(stack);
								break;
							}
						}
					}
				}
			}

		if(tile.atTickRate(20))
			neighborChanged(tile);

		super.update(tile);
	}

	public FluidStack gatherGrid()
	{
		AtomicReference<FluidStack> stack = new AtomicReference<>();
		if(tank.getFluid() != null && getGrid() != null)
		{
			getGrid().streamOfPipes(FluidPipe.class).forEach(pipe ->
			{
				FluidStack fs = stack.get();
				if(pipe.tank.getFluid() != null && (fs == null || pipe.tank.getFluid().isFluidEqual(fs)))
				{
					if(fs == null)
						fs = pipe.tank.getFluid().copy();
					else
						fs.amount += pipe.tank.getFluidAmount();
				}
				stack.set(fs);
			});
		}
		return stack.get();
	}

	public void drainGrid(FluidStack fluidStack)
	{
		AtomicReference<FluidStack> stack = new AtomicReference<>(fluidStack);
		if(getGrid() != null)
		{
			getGrid().streamOfPipes(FluidPipe.class).forEach(pipe ->
			{
				FluidStack fs = stack.get();
				if(fs != null && fs.amount > 0 && pipe.tank.getFluid() != null && pipe.tank.getFluid().isFluidEqual(fs))
				{
					FluidStack fs2 = pipe.tank.drain(fs, true);
					if(fs2 != null)
						fs.amount -= fs2.amount;
				}
				stack.set(fs);
			});
		}
	}

	public FluidStack fillGrid(FluidStack fluidStack)
	{
		AtomicReference<FluidStack> stack = new AtomicReference<>(fluidStack);
		if(getGrid() != null)
		{
			getGrid().streamOfPipes(FluidPipe.class).forEach(pipe ->
			{
				FluidStack fs = stack.get();
				if(fs != null && fs.amount > 0 && (pipe.tank.getFluidAmount() == 0 || (pipe.tank.getFluid() != null && pipe.tank.getFluid().isFluidEqual(fs))))
				{
					int fs2 = pipe.tank.fill(fs, true);
					if(fs2 > 0)
						fs.amount -= fs2;
				}
				stack.set(fs);
			});
		}
		return stack.get();
	}

	@Override
	public void balanceGrid()
	{
		PipeGrid pg = getGrid();
		if(pg != null)
		{
			List<FluidTank> tanks = new ArrayList<>();
			pg.streamOfPipes(FluidPipe.class).forEach(fp -> tanks.add(fp.tank));

			Map<Fluid, List<FluidTank>> grouped = new HashMap<>();
			tanks.forEach(tank ->
			{
				if(tank.getFluidAmount() > 0)
					grouped.computeIfAbsent(tank.getFluid().getFluid(), k -> new ArrayList<>()).add(tank);
			});

			grouped.values().forEach(tlist ->
			{
				for(int i = 0; i < tlist.size(); ++i)
				{
					FluidTank tank = tlist.get(i);
					FluidStack stack = tank.getFluid();
				}
			});
		}
	}

	@Override
	public boolean testConnectionTo(EnumFacing facing, IPipe other)
	{
		TileEntity te = world.getTileEntity(pos.offset(facing));
		if(te != null && te.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, facing.getOpposite()))
			return true;
		return other != null && other instanceof FluidPipe;
	}

	@Override
	public <T> boolean hasCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ? (T) tank : null;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}

	SimpleProgressBar fluidBar = new SimpleProgressBar();

	@Override
	@SideOnly(Side.CLIENT)
	public void addTooltip(ITooltip tooltip)
	{
		int en = Math.min(capacity, tank.getFluidAmount());
		fluidBar.setProgress(en / (float) capacity);

		int rgb = MathHelper.hsvToRGB(.999F - (fluidBar.getProgressPercent() / 100F) / 2F, 1F, 1F);

		fluidBar.filledMainColor = 0xFF00FFFF;
		fluidBar.filledAlternateColor = 0xFF009696;
		fluidBar.backgroundColor = ColorHelper.packARGB(1F, ColorHelper.getRed(rgb) * .35F, ColorHelper.getGreen(rgb) * .35F, ColorHelper.getBlue(rgb) * .35F);
		fluidBar.borderColor = 0xFF757575;

		StringTooltipInfo i = new StringTooltipInfo(String.format("%,d", en));
		i.color = rgb;
		tooltip.append(i);
		tooltip.append(new StringTooltipInfo(String.format(" / %,d MB" + (tank.getFluid() != null ? (" of " + tank.getFluid().getLocalizedName()) : "") + ":", capacity)));
		tooltip.newLine();
		tooltip.append(new ProgressBarTooltipInfo(fluidBar));
	}
}