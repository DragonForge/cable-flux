package org.zeith.cableflux.util;

import java.util.ArrayList;
import java.util.List;

public class Scheduler<T>
{
	private final List<Task<T>> queue = new ArrayList<>();
	
	public void schedule(int delay, IScheduledTask<T> task)
	{
		queue.add(new Task<>(task, delay));
	}
	
	public void update(T data)
	{
		if(!queue.isEmpty())
		{
			Task<T> task = queue.get(0);
			if(task.tick(data))
				queue.remove(0);
		}
	}
	
	private static class Task<T>
	{
		IScheduledTask<T> task;
		int act;
		
		public Task(IScheduledTask<T> task, int act)
		{
			this.task = task;
			this.act = act;
		}
		
		boolean tick(T data)
		{
			if(--act <= 0)
			{
				task.doWork(data);
				return true;
			}
			return false;
		}
	}
	
	public interface IScheduledTask<T>
	{
		void doWork(T data);
	}
}