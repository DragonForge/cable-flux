package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.client.utils.RenderBlocks;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * An additional implementation to {@link IPipeImage} to allow custom render
 * control over a pipe.
 */
public interface IPipeRenderer
{
	/**
	 * Does all the custom rendering instead of the vanilla pipe model. The
	 * tessellation is already began when this method runs and ends after this
	 * method call.
	 */
	@SideOnly(Side.CLIENT)
	void doRender(IPipeImage pipe, RenderBlocks renderBlocks, int bright, float alpha, float partialTicks);
}