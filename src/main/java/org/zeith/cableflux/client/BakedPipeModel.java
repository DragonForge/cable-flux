package org.zeith.cableflux.client;

import com.google.common.base.Predicates;
import com.zeitheron.hammercore.utils.PositionedStateImplementation;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.util.vector.Vector3f;
import org.zeith.cableflux.blocks.BlockPipe;
import org.zeith.cableflux.pipes.IPipe;
import org.zeith.cableflux.pipes.PipeManager;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class BakedPipeModel
		implements IBakedModel
{
	public static final FaceBakery $ = new FaceBakery();

	BlockPipe pipe;

	public BakedPipeModel(BlockPipe pipe)
	{
		this.pipe = pipe;
	}

	@Override
	public List<BakedQuad> getQuads(@Nullable IBlockState state, @Nullable EnumFacing side, long rand)
	{
		if(side != null)
		{
			PositionedStateImplementation extendedState = Cast.cast(state, PositionedStateImplementation.class);
			if(extendedState == null)
				return Collections.emptyList();
			IBlockAccess world = extendedState.getWorld();
			BlockPos pos = extendedState.getPos();
			IPipe pipe = PipeManager.getPipe(world, pos);
			Predicate<EnumFacing> ctp = pipe == null ? Predicates.alwaysFalse() : pipe::isConnectedTo;
			List<BakedQuad> quads = new ArrayList<>();
			TextureAtlasSprite tx = getParticleTexture();
			if(pipe != null)
				pipe.generateBakedQuads(quads, side, rand, world, pos, extendedState, ctp, tx);
			else
				generateQuads(quads, side, rand, world, pos, extendedState, ctp, tx);
			return quads;
		}
		return Collections.emptyList();
	}

	public static void generateQuads(List<BakedQuad> quads, EnumFacing side, long rand, IBlockAccess world, BlockPos pos, PositionedStateImplementation state, Predicate<EnumFacing> ctp, TextureAtlasSprite tx)
	{
		List<EnumFacing> connected = new ArrayList<>();
		for(EnumFacing f : EnumFacing.VALUES)
			if(ctp.test(f))
				connected.add(f);

		if(connected.size() == 2 && connected.get(0).getOpposite() == connected.get(1) && world.getBlockState(pos.offset(connected.get(0))).getBlock() == state.getBlock() && world.getBlockState(pos.offset(connected.get(1))).getBlock() == state.getBlock())
		{
			EnumFacing.Axis axial = connected.get(0).getAxis();

			if(axial == EnumFacing.Axis.X && side.getAxis() != EnumFacing.Axis.X)
				quads.add($.makeBakedQuad(
						new Vector3f(5, 5.5F, 5.5F), new Vector3f(11, 10.5F, 10.5F),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(new float[]
										{
												6,
												0,
												12,
												5
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						false,
						true));

			if(axial == EnumFacing.Axis.Y && side.getAxis() != EnumFacing.Axis.Y)
				quads.add($.makeBakedQuad(
						new Vector3f(5.5F, 5, 5.5F), new Vector3f(10.5F, 11, 10.5F),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(new float[]
										{
												0,
												6,
												5,
												12
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						false,
						true));

			if(axial == EnumFacing.Axis.Z && side.getAxis() != EnumFacing.Axis.Z)
				quads.add($.makeBakedQuad(
						new Vector3f(5.5F, 5.5F, 5), new Vector3f(10.5F, 10.5F, 11),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
										{
												0,
												6,
												5,
												12
										} : new float[]
										{
												6,
												0,
												12,
												5
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						true,
						true));
		} else
			quads.add($.makeBakedQuad(
					new Vector3f(5, 5, 5), new Vector3f(11, 11, 11),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											0,
											6,
											6
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.WEST) && side.getAxis() != EnumFacing.Axis.X)
			quads.add($.makeBakedQuad(
					new Vector3f(0, 5.5F, 5.5F), new Vector3f(5, 10.5F, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.EAST) && side.getAxis() != EnumFacing.Axis.X)
			quads.add($.makeBakedQuad(
					new Vector3f(11, 5.5F, 5.5F), new Vector3f(16, 10.5F, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.DOWN) && side.getAxis() != EnumFacing.Axis.Y)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 0, 5.5F), new Vector3f(10.5F, 5, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											6,
											5,
											11
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.UP) && side.getAxis() != EnumFacing.Axis.Y)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 11, 5.5F), new Vector3f(10.5F, 16, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											6,
											5,
											11
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.NORTH) && side.getAxis() != EnumFacing.Axis.Z)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 5.5F, 0), new Vector3f(10.5F, 10.5F, 5),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
									{
											0,
											6,
											5,
											11
									} : new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));

		if(connected.contains(EnumFacing.SOUTH) && side.getAxis() != EnumFacing.Axis.Z)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 5.5F, 11), new Vector3f(10.5F, 10.5F, 16),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
									{
											0,
											6,
											5,
											11
									} : new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					false,
					true));
	}

	@Override
	public boolean isAmbientOcclusion()
	{
		return false;
	}

	@Override
	public boolean isGui3d()
	{
		return false;
	}

	@Override
	public boolean isBuiltInRenderer()
	{
		return true;
	}

	@Override
	public TextureAtlasSprite getParticleTexture()
	{
		return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(pipe.getTexture().toString());
	}

	@Override
	public ItemOverrideList getOverrides()
	{
		return null;
	}
}