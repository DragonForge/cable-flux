package org.zeith.cableflux;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zeith.cableflux.init.BlocksCF;
import org.zeith.cableflux.proxy.CommonProxy;

@Mod(modid = "cableflux", name = "Cable Flux", version = "@VERSION@", dependencies = "required-after:hammercore", certificateFingerprint = "9f5e2a811a8332a842b34f6967b7db0ac4f24856", guiFactory = "org.zeith.cableflux.cfg.ConfigFactoryCF", updateJSON = "http://dccg.herokuapp.com/api/fmluc/303824")
public class CableFlux
{
	@SidedProxy(clientSide = "org.zeith.cableflux.proxy.ClientProxy", serverSide = "org.zeith.cableflux.proxy.CommonProxy")
	public static CommonProxy proxy;
	public static final Logger LOG = LogManager.getLogger("CableFlux");
	public static final CreativeTabs tab = new CreativeTabs("cableflux")
	{
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(BlocksCF.FE_3);
		}
	};
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with CableFlux jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://dccg.herokuapp.com/api/fmlhp/303824 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put("cableflux", "https://dccg.herokuapp.com/api/fmlhp/303824");
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		LOG.info("*** PRE-LOADING CABLE FLUX. ***");
		
		proxy.preInit();
		LOG.info("WE HOPE THAT YOU HAD DOWNLOADED IT FROM THE OFFICIAL PAGE.");
		
		SimpleRegistration.registerFieldBlocksFrom(BlocksCF.class, "cableflux", tab);
		LOG.info("https://dccg.herokuapp.com/api/fmlhp/303824/");
		
		LOG.info("*** *********************** ***");
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
}