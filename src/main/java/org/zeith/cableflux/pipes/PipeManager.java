package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import org.zeith.cableflux.tiles.TilePipe;

public class PipeManager
{
	public static IPipe getPipe(IBlockAccess world, BlockPos pos)
	{
		if(world == null || pos == null)
			return null;
		TilePipe holder = Cast.cast(world.getTileEntity(pos), TilePipe.class);
		if(holder != null)
			return holder.getPipe();
		return null;
	}
}