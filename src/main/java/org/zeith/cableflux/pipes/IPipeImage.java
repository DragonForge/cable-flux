package org.zeith.cableflux.pipes;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

/**
 * A class that stores the base info about the pipe. It may be implemented from
 * Block or IPipe to allow control over texture and connected elements of the
 * model.
 */
public interface IPipeImage
{
	/**
	 * Gets the path to a texture object representing this pipe.
	 */
	ResourceLocation getTexture();

	/**
	 * Checks if this pipe is currently connected to the given side.
	 */
	boolean isConnectedTo(EnumFacing face);
}