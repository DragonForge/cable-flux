package org.zeith.cableflux.blocks;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.api.blocks.INoBlockstate;
import com.zeitheron.hammercore.api.inconnect.InConnectAPI;
import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.ICubeManager;
import com.zeitheron.hammercore.internal.blocks.IItemBlock;
import com.zeitheron.hammercore.utils.base.Cast;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.pipes.IPipe;
import org.zeith.cableflux.pipes.IPipeImage;
import org.zeith.cableflux.pipes.PipeManager;
import org.zeith.cableflux.tiles.TilePipe;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class BlockPipe
		extends BlockTraceable
		implements ITileBlock<TilePipe>, ICubeManager, IPipeImage, INoBlockstate
{
	private final Supplier<IPipe> ptype;
	public ResourceLocation texture;
	
	public BlockPipe(Material mat, Supplier<IPipe> type)
	{
		super(mat);
		this.ptype = type;
		setHardness(0F);
		setHarvestLevel("pickaxe", 0);
	}
	
	public IPipe createPipe(TilePipe tile)
	{
		IPipe pipe = ptype.get();
		pipe.initTile(tile);
		return pipe;
	}
	
	@Override
	public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return InConnectAPI.makeExtendedPositionedState(world, pos, state);
	}
	
	@Override
	public BlockPipe setTranslationKey(String name)
	{
		super.setTranslationKey(name);
		return this;
	}
	
	@Override
	public Class<TilePipe> getTileClass()
	{
		return TilePipe.class;
	}
	
	@Override
	public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
	{
		TilePipe tile = Cast.cast(world.getTileEntity(pos), TilePipe.class);
		if(tile != null)
			tile.scheduleConnectionRefresh(2);
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
	{
		TilePipe tile = Cast.cast(worldIn.getTileEntity(pos), TilePipe.class);
		if(tile != null)
			tile.scheduleConnectionRefresh(2);
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TilePipe tile = Cast.cast(worldIn.getTileEntity(pos), TilePipe.class);
		if(tile != null && tile.pipe != null)
			tile.pipe.createDrops(tile.getLocation());
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player)
	{
		return true;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face)
	{
		return BlockFaceShape.UNDEFINED;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		return true;
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	private static final ThreadLocal<List<Cuboid6>> cubeLists = ThreadLocal.withInitial(ArrayList::new);
	
	@Override
	public Cuboid6[] getCuboids(World world, BlockPos pos, IBlockState state)
	{
		IPipe pipe = PipeManager.getPipe(world, pos);
		if(pipe != null)
		{
			List<Cuboid6> c = cubeLists.get();
			c.clear();
			pipe.addCuboids(c);
			return c.toArray(new Cuboid6[c.size()]);
		}
		return new Cuboid6[0];
	}
	
	public BlockPipe setTexture(ResourceLocation texture)
	{
		this.texture = texture;
		return this;
	}
	
	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}
	
	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return face.getAxis() == Axis.Y;
	}
}