package org.zeith.cableflux.cfg;

import org.zeith.cableflux.CableFlux;
import com.zeitheron.hammercore.cfg.gui.HCConfigGui;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;

import java.util.Set;

public class ConfigFactoryCF
		implements IModGuiFactory
{
	@Override
	public void initialize(Minecraft minecraftInstance)
	{
		CableFlux.LOG.info("Created Cable Flux Gui Config Factory!");
	}

	@Override
	public boolean hasConfigGui()
	{
		return true;
	}

	@Override
	public GuiScreen createConfigGui(GuiScreen parentScreen)
	{
		return new HCConfigGui(parentScreen, ConfigCF.cfg, "cableflux");
	}

	@Override
	public Set<RuntimeOptionCategoryElement> runtimeGuiCategories()
	{
		return null;
	}
}