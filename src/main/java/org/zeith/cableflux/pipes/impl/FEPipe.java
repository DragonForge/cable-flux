package org.zeith.cableflux.pipes.impl;

import com.zeitheron.hammercore.tile.tooltip.SimpleProgressBar;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ProgressBarTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.hammercore.utils.math.BigMath;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.cableflux.pipes.BasePipe;
import org.zeith.cableflux.pipes.IPipe;
import org.zeith.cableflux.pipes.PipeGrid;
import org.zeith.cableflux.tiles.TilePipe;
import org.zeith.cableflux.util.HumongousFEStorage;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class FEPipe
		extends BasePipe
{
	public ResourceLocation texture;
	public BigInteger capacity, throughput;
	public HumongousFEStorage storage;
	public Set<BlockPos> energyDemand = new HashSet<>();
	
	public FEPipe(ResourceLocation texture, Number capacity, Number throughput)
	{
		this(texture, new BigInteger(Long.toString(capacity.longValue())), new BigInteger(Long.toString(throughput.longValue())));
	}
	
	public FEPipe(ResourceLocation texture, BigInteger capacity, BigInteger throughput)
	{
		this.texture = texture;
		this.storage = new HumongousFEStorage(capacity, throughput);
		this.capacity = capacity;
		this.throughput = throughput;
	}
	
	public FEPipe setTexture(ResourceLocation texture)
	{
		this.texture = texture;
		return this;
	}
	
	@Override
	public void update(TilePipe tile)
	{
		if(world != null && !world.isRemote)
		{
			boolean doWeNeedPower = false;
			
			for(EnumFacing face : EnumFacing.VALUES)
			{
				BlockPos pos = tile.getPos().offset(face);
				TileEntity te = tile.getWorld().getTileEntity(pos);
				
				IEnergyStorage es = null;
				
				if(te != null && !(te instanceof TilePipe))
				{
					if(te.hasCapability(CapabilityEnergy.ENERGY, face.getOpposite()))
						es = te.getCapability(CapabilityEnergy.ENERGY, face.getOpposite());
					else try
					{
						if(te.hasCapability(CapabilityEnergy.ENERGY, null))
							es = te.getCapability(CapabilityEnergy.ENERGY, null);
					} catch(Throwable e)
					{
						// SOME mods can't handle null faces, others can with ease.
					}
				}
				
				if(es != null)
				{
					if(es.canExtract() || !energyDemand.isEmpty())
					{
						int canExtract = storage.getMaxEnergyStored() - storage.getEnergyStored();
						canExtract = es.extractEnergy(canExtract, true);
						storage.receiveEnergy(es.extractEnergy(canExtract, false), false);
					}
					
					if(es.canReceive())
					{
						if(es.receiveEnergy(BigMath.trimToInt(throughput), true) > 0
								&& es.extractEnergy(BigMath.trimToInt(throughput), true) <= 0)
							doWeNeedPower = true; // Create a demand for energy
						
						int canReceive = storage.getEnergyStored();
						canReceive = es.receiveEnergy(canReceive, true);
						storage.extractEnergy(es.receiveEnergy(canReceive, false), false);
					}
				}
			}
			
			if(doWeNeedPower)
				energyDemand.add(pos);
			else
				energyDemand.remove(pos);
		}
		
		super.update(tile);
	}
	
	@Override
	public void balanceGrid()
	{
		PipeGrid pg = getGrid();
		if(pg != null && !world.isRemote)
		{
			AtomicReference<BigInteger> LEFTOVER = new AtomicReference<>(BigInteger.ZERO);
			
			List<FEPipe> allPipes = pg.streamOfPipes(FEPipe.class).collect(Collectors.toList());
			
			allPipes.forEach(fe ->
			{
				LEFTOVER.updateAndGet(p -> p.add(fe.storage.getEnergy()));
				fe.storage.setEnergy(BigInteger.ZERO);
				fe.energyDemand = energyDemand;
			});
			
			if(allPipes.size() != pg.pipes.size())
			{
				pg.majorGridChange();
				return;
			}
			
			List<FEPipe> demandingPipes = allPipes.stream()
					.filter(fe -> energyDemand.contains(fe.pos))
					.collect(Collectors.toList());
			
			// Prioritize the energy to demanding pipes first, then balance out the rest of the energy.
			if(!demandingPipes.isEmpty())
			{
				// Perform energy split, fill all sink pipes first, leaving out the rest of the energy.
				BigInteger ndpcBI = BigInteger.valueOf(demandingPipes.size());
				BigInteger fePer = BigMath.max(BigInteger.ZERO, LEFTOVER.get().divide(ndpcBI));
				demandingPipes.forEach(fe ->
				{
					BigInteger accept = BigMath.min(fe.storage.getCapacity().subtract(fe.storage.getEnergy()), fePer);
					fe.storage.setEnergy(accept);
					
					LEFTOVER.set(LEFTOVER.get().subtract(accept));
				});
			}
			
			int ndpc = allPipes.size() - demandingPipes.size();
			if(ndpc > 0)
			{
				// Perform rest energy split after sink pipes between all neutral pipes, leaving out the rest due to uneven divisions.
				BigInteger ndpcBI = BigInteger.valueOf(ndpc);
				BigInteger fePer = BigMath.max(BigInteger.ZERO, LEFTOVER.get().divide(ndpcBI));
				allPipes.stream()
						.filter(fe -> !demandingPipes.contains(fe))
						.forEach(fe ->
						{
							BigInteger set = BigMath.min(fe.storage.getCapacity().subtract(fe.storage.getEnergy()), fePer);
							fe.storage.setEnergy(set);
							
							LEFTOVER.set(LEFTOVER.get().subtract(set));
						});
			}
			
			// We have leftover power left, push it to whoever can receive it.
			if(BigMath.isAGreaterThenB(LEFTOVER.get(), BigInteger.ZERO, true))
				for(FEPipe fe : allPipes)
				{
					BigInteger canAccept = fe.storage.getCapacity().subtract(fe.storage.getEnergy());
					BigInteger willAccept = BigMath.min(canAccept, LEFTOVER.get());
					
					fe.storage.setEnergy(fe.storage.getEnergy().add(willAccept));
					
					LEFTOVER.set(LEFTOVER.get().subtract(willAccept));
					
					if(BigMath.max(LEFTOVER.get(), BigInteger.ZERO).equals(BigInteger.ZERO))
						break;
				}
		}
	}
	
	@Override
	public void write(NBTTagCompound nbt)
	{
		super.write(nbt);
		storage.writeToNBT(nbt);
	}
	
	@Override
	public void read(NBTTagCompound nbt)
	{
		super.read(nbt);
		storage.readFromNBT(nbt);
	}
	
	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}
	
	@Override
	public boolean testConnectionTo(EnumFacing facing, IPipe other)
	{
		TileEntity te = world.getTileEntity(pos.offset(facing));
		if(te != null)
		{
			if(te.hasCapability(CapabilityEnergy.ENERGY, facing.getOpposite()))
			{
				IEnergyStorage c = te.getCapability(CapabilityEnergy.ENERGY, facing.getOpposite());
				return c != null && (c.canExtract() || c.canReceive());
			}
		}
		
		return other instanceof FEPipe;
	}
	
	@Override
	public <T> boolean hasCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY ? CapabilityEnergy.ENERGY.cast(storage) : super.getCapability(capability, facing);
	}
	
	SimpleProgressBar energyBar = new SimpleProgressBar();
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addTooltip(ITooltip tooltip)
	{
		BigInteger en = BigMath.min(capacity, storage.getEnergy());
		energyBar.setProgress(new BigDecimal(en).divide(new BigDecimal(capacity)).floatValue());
		
		int rgb = MathHelper.hsvToRGB(.999F - (energyBar.getProgressPercent() / 100F) / 2F, 1F, 1F);
		
		energyBar.filledMainColor = 0xFFFF0000;
		energyBar.filledAlternateColor = 0xFF960000;
		energyBar.backgroundColor = ColorHelper.packARGB(1F, ColorHelper.getRed(rgb) * .35F, ColorHelper.getGreen(rgb) * .35F, ColorHelper.getBlue(rgb) * .35F);
		energyBar.borderColor = 0xFF757575;
		
		StringTooltipInfo i = new StringTooltipInfo(String.format("%,d", en));
		i.color = rgb;
		tooltip.append(i);
		tooltip.append(new StringTooltipInfo(String.format(" / %,d FE:", capacity)));
		tooltip.newLine();
		tooltip.append(new ProgressBarTooltipInfo(energyBar));
	}
}