package org.zeith.cableflux.pipes;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import org.zeith.cableflux.net.PacketPerformPipeSync;
import org.zeith.cableflux.net.PacketRequestPipeSync;
import org.zeith.cableflux.tiles.TilePipe;

/**
 * Use to sync pipe info.
 */
public class PipeSync
{
	/**
	 * Client-side call to request pipe sync from server.
	 */
	public static void requestSyncFromClient(TilePipe pipe)
	{
		HCNet.INSTANCE.sendToServer(new PacketRequestPipeSync(pipe.getPos()));
	}
	
	/**
	 * Client-side call to request pipe sync from server.
	 */
	public static void requestSyncFromClient(IPipe pipe)
	{
		HCNet.INSTANCE.sendToServer(PacketRequestPipeSync.create(pipe));
	}
	
	public static void sendSyncToClients(IPipe pipe)
	{
		WorldServer world = Cast.cast(pipe.getWorld(), WorldServer.class);
		BlockPos pos = pipe.getCoords();
		
		TilePipe holder = Cast.cast(world.getTileEntity(pos), TilePipe.class);
		if(holder != null)
		{
			PacketPerformPipeSync pkt = new PacketPerformPipeSync();
			pkt.nbt = holder.getUpdateTag();
			pkt.pos = pos.toLong();
			HCNet.INSTANCE.sendToAllAround(pkt, holder.getSyncPoint(128));
		}
	}
}