package org.zeith.cableflux.init;

import org.zeith.cableflux.blocks.BlockPipe;
import org.zeith.cableflux.pipes.impl.FluidPipe;
import org.zeith.cableflux.pipes.impl.FEPipe;
import net.minecraft.block.material.Material;

public class BlocksCF
{
	public static final BlockPipe FE_1 = new BlockPipe(Material.ROCK, () -> new FEPipe(TexturesCF.FE_1, 320, 320)).setTexture(TexturesCF.FE_1).setTranslationKey("fec_1");
	public static final BlockPipe FE_2 = new BlockPipe(Material.ROCK, () -> new FEPipe(TexturesCF.FE_2, 3200, 3200)).setTexture(TexturesCF.FE_2).setTranslationKey("fec_2");
	public static final BlockPipe FE_3 = new BlockPipe(Material.ROCK, () -> new FEPipe(TexturesCF.FE_3, 320_000, 320_000)).setTexture(TexturesCF.FE_3).setTranslationKey("fec_3");

//	public static final BlockPipe FL_1 = new BlockPipe(Material.ROCK, () -> new FluidPipe(TexturesCF.FL_1, 100)).setTexture(TexturesCF.FL_1).setTranslationKey("flc_1");
}